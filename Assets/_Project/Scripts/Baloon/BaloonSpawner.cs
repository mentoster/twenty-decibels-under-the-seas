using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace RtuItLab
{
    public class BaloonSpawner : MonoBehaviour
    {
        public GameObject objectToSpawn; // Assign the prefab in the inspector

        [BoxGroup("Spawn Timing")]
        public float minSpawnInterval = 1f; // Minimum spawn interval
        [BoxGroup("Spawn Timing")]
        public float maxSpawnInterval = 5f; // Maximum spawn interval

        [BoxGroup("Spawn Area")]
        [MinMaxSlider(-125f, 125f)] // Use NaughtyAttributes to define a slider in the inspector
        public Vector2 spawnXBounds = new(-125f, 125f);
        [BoxGroup("Spawn Area")]
        [MinMaxSlider(-125f, 125f)]
        public Vector2 spawnZBounds = new(-125f, 125f);

        private float _timer; // Tracks the time to the next spawn

        private void Start()
        {
            SetRandomTimer(); // Initialize the timer with a random value
            SpawnObject(); // Spawn an object immediately
        }

        private void Update()
        {
            _timer -= Time.deltaTime; // Decrement the timer by the elapsed time since the last frame

            if (_timer <= 0)
            {
                SpawnObject();
                SetRandomTimer(); // Reset the timer with a new random value
            }
        }

        void SpawnObject()
        {
            // Generate a random position within the specified bounds
            var spawnPosition = new Vector3(
                Random.Range(spawnXBounds.x, spawnXBounds.y),
                0, // Adjust this Y coordinate as needed for your scene
                Random.Range(spawnZBounds.x, spawnZBounds.y)
            );

            // Spawn the object at the generated position without any rotation
            Instantiate(objectToSpawn, spawnPosition, Quaternion.identity);
        }

        void SetRandomTimer()
        {
            _timer = Random.Range(minSpawnInterval, maxSpawnInterval); // Set timer to a random value between min and max
        }
    }
}
