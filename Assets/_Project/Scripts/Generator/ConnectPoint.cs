using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace RtuItLab
{
    public class ConnectPoint : MonoBehaviour
    {
        [ReadOnly] // Makes the field read-only in the editor
        public bool isConnected;

        [ShowIf("IsConnected"), ReadOnly] // Only shows this field if IsConnected is true
        public GameObject connectedRoom;

        // Example method to visually represent if a ConnectPoint is connected in the editor
        public void ConnectTo(GameObject otherRoom)
        {
            connectedRoom = otherRoom;
            isConnected = true;
        }


    }
}
