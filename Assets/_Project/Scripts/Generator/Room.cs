using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace RtuItLab
{

    public class Room : MonoBehaviour
    {
        public List<ConnectPoint> connectPoints;

        // Метод, вызываемый при старте игры
        private void Awake()
        {
            FindConnectPoints();
        }

        // Метод для нахождения всех ConnectPoint
        private void FindConnectPoints()
        {
            connectPoints = new List<ConnectPoint>();
            connectPoints.AddRange(GetComponentsInChildren<ConnectPoint>());
        }

        // Метод для ручного вызова в редакторе
        [Button("Find Connect Points")]
        private void FindConnectPointsButton()
        {
            FindConnectPoints();
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(this); // Помечаем объект как измененный, чтобы сохранить изменения
#endif
        }
    }
}
