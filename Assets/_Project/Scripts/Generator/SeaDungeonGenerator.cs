using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using NaughtyAttributes;
using UnityEngine;

namespace RtuItLab
{
    public class SeaDungeonGenerator : MonoBehaviour
    {
        [Header("Dungeon Rooms")]
        [SerializeField] private GameObject _startZonePrefab; // Start zone prefab to be instantiated first
        [SerializeField] private List<GameObject> _roomPrefabs; // List of room prefabs to be instantiated
        [SerializeField] private GameObject _endZonePrefab; // End zone prefab to be instantiated last

        [SerializeField] private int _numberOfRooms; // Number of rooms to generate
        [BoxGroup("Dungeon Generation Settings")]
        [SerializeField] private int _seed = 0; // Seed for the random number generator

        private void Start()
        {
            if (_seed == 0)
            {
                _seed = Random.Range(0, 10000); // Randomize the seed if it's not set
            }

            GenerateDungeon();
        }

        private void GenerateDungeon()
        {
            Random.InitState(_seed); // Initialize random number generator with the seed
            var generatedRooms = new List<GameObject>();
            var availableConnectPoints = new List<ConnectPoint>();

            // Instantiate the start zone as the initial part of the dungeon
            if (_startZonePrefab != null)
            {
                var startZoneInstance = Instantiate(_startZonePrefab, transform);
                var startZone = startZoneInstance.GetComponent<Room>();
                if (startZone == null || startZone.connectPoints == null || startZone.connectPoints.Count == 0)
                {
                    Debug.LogError("Start Zone or ConnectPoints not properly initialized.");
                    return;
                }
                availableConnectPoints.AddRange(startZone.connectPoints);
                generatedRooms.Add(startZoneInstance);
            }
            else
            {
                Debug.LogError("Start Zone prefab is not assigned.");
                return;
            }

            // Continue generating rooms as before, starting from the connect points of the start zone
            while (availableConnectPoints.Count > 0 && generatedRooms.Count < _numberOfRooms + 1) // +1 to account for the start zone
            {
                var connectPoint = availableConnectPoints[Random.Range(0, availableConnectPoints.Count)];

                if (connectPoint.isConnected)
                {
                    availableConnectPoints.Remove(connectPoint);
                    continue;
                }

                var roomPrefab = _roomPrefabs[Random.Range(0, _roomPrefabs.Count)];
                var roomInstance = Instantiate(roomPrefab, transform);
                var room = roomInstance.GetComponent<Room>();

                if (generatedRooms.Count > 0) // Always true in this context, kept for clarity
                {
                    ConnectRooms(connectPoint, room);
                    availableConnectPoints.Remove(connectPoint);
                    availableConnectPoints.AddRange(room.connectPoints);
                }

                generatedRooms.Add(roomInstance);
            }
        }


        private void ConnectRooms(ConnectPoint connectPoint, Room currentRoom)
        {
            foreach (var currRoomPoint in currentRoom.connectPoints)
            {
                if (!currRoomPoint.isConnected)
                {
                    var directionToPrev = (connectPoint.transform.position - currRoomPoint.transform.position).normalized;
                    var fromToRotation = Quaternion.FromToRotation(currRoomPoint.transform.forward, directionToPrev);
                    currentRoom.transform.rotation = fromToRotation * currentRoom.transform.rotation;

                    var offset = connectPoint.transform.position - currRoomPoint.transform.position;
                    currentRoom.transform.position += offset;

                    connectPoint.isConnected = true;
                    currRoomPoint.isConnected = true;

                    return; // Exit the loop once a connection is made.
                }
            }
        }

    }
}
