using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

namespace RtuItLab
{
    public class OxygenPickup : MonoBehaviour
    {
        [BoxGroup("Oxygen Pickup Settings")]
        [MinMaxSlider(0, 300)] // Use NaughtyAttributes to create a slider in the Inspector
        public Vector2 oxygenAmountRange = new(40, 60); // The range of oxygen to add
        private OxygenController _oxygenController;
        private void Start()
        {
            _oxygenController = FindObjectOfType<OxygenController>(); // Find the OxygenController in the scene
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Baloon")) // Check if the collider is tagged as Player
            {
                var amountToAdd = Random.Range(oxygenAmountRange.x, oxygenAmountRange.y);
                _oxygenController.AddOxygen(amountToAdd); // Add a random amount of oxygen within the range
                Destroy(other.gameObject); // Optionally destroy the pickup after it's used
            }
        }
    }
}
