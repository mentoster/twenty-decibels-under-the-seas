using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RtuItLab
{

    public class OxygenController : MonoBehaviour
    {
        [Header("UI Components")]
        [SerializeField]
        private TextMeshProUGUI _oxygenText; // Assign in the inspector
        [SerializeField]
        private Image _oxygenImage; // Assign in the inspector

        [Header("Oxygen Settings")]
        [SerializeField]
        private float _depletionTime = 40f; // Time in seconds for oxygen to deplete, editable in inspector

        private float _oxygenLevel = 100f; // Starting oxygen level
        private float _maxOxygen = 100f; // The maximum oxygen level
        private float _oxygenDecreaseRate; // Calculated based on the desired duration to deplete oxygen

        private void Start()
        {
            // Calculate the rate at which oxygen should decrease to reach 0 in the specified depletion time
            _oxygenDecreaseRate = _maxOxygen / _depletionTime;
        }

        private void Update()
        {
            // Decrease oxygen level over time
            if (_oxygenLevel > 0)
            {
                _oxygenLevel -= _oxygenDecreaseRate * Time.deltaTime;
                _oxygenLevel = Mathf.Max(_oxygenLevel, 0); // Ensure oxygen doesn't go below 0
            }

            // Update the TextMeshPro text to display the current oxygen level as a percentage
            if (_oxygenText != null)
            {
                _oxygenText.text = $"Oxygen: {_oxygenLevel.ToString("0.00")}%";
            }

            // Update the Slider to reflect the current oxygen level
            if (_oxygenImage != null)
            {
                _oxygenImage.fillAmount = _oxygenLevel / _maxOxygen;
            }
        }
        public void AddOxygen(float amount)
        {
            _oxygenLevel += amount;
            _oxygenLevel = Mathf.Clamp(_oxygenLevel, 0, _maxOxygen); // Ensure the oxygen level does not exceed the maximum
        }
    }
}
