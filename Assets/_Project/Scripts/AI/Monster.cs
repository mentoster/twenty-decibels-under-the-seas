using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace RtuItLab
{
    public class Monster : MonoBehaviour
    {
        public UnityEngine.AI.NavMeshAgent agent;
        public Transform player;
        public LayerMask whatIsGround, whatIsPlayer;

        [BoxGroup("Patrol Settings")]
        public Vector3 walkPoint;
        bool walkPointSet;
        [BoxGroup("Patrol Settings")]
        [MinMaxSlider(0, 100)]
        public float walkPointRange;

        [BoxGroup("Attack Settings")]
        [MinMaxSlider(0, 10)]
        public float timeBetweenAttacks;
        bool alreadyAttacked;

        [BoxGroup("Sight Settings")]
        [MinMaxSlider(0, 100)]
        public float sightRange, attackRange;
        [BoxGroup("Sight Settings")]
        public bool playerInSightRange, playerInAttackRange;

        [BoxGroup("Speed Settings")]
        public float patrolSpeed = 3.5f;
        [BoxGroup("Speed Settings")]
        public float chaseSpeed = 7f;
        AtackAnimationStarter _attackAnimationStarter;
        private void Awake()
        {
            agent.speed = patrolSpeed; // Set initial speed to patrol speed
            _attackAnimationStarter = GetComponent<AtackAnimationStarter>();
        }

        private void Update()
        {
            // Check for sight and attack range
            playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
            playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

            if (playerInSightRange && playerInAttackRange)
            {
                AttackPlayer();
            }
            else
            {
                if (playerInSightRange && !playerInAttackRange)
                {
                    ChasePlayer();
                }
                else
                {

                    if (!playerInSightRange && !playerInAttackRange)
                    {
                        Patrolling();
                    }
                }
            }

        }

        private void Patrolling()
        {
            if (!walkPointSet) SearchWalkPoint();

            if (walkPointSet)
                agent.SetDestination(walkPoint);

            Vector3 distanceToWalkPoint = transform.position - walkPoint;

            // Walkpoint reached
            if (distanceToWalkPoint.magnitude < 1f)
                walkPointSet = false;

            agent.speed = patrolSpeed; // Reset speed to patrol speed when patrolling
        }

        private void ChasePlayer()
        {
            agent.SetDestination(player.position);
            agent.speed = chaseSpeed; // Increase speed when chasing the player
        }

        private void SearchWalkPoint()
        {
            // Calculate random point in range
            float randomZ = Random.Range(-walkPointRange, walkPointRange);
            float randomX = Random.Range(-walkPointRange, walkPointRange);

            walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

            if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
                walkPointSet = true;
        }

        private void AttackPlayer()
        {
            // Ensure the alien doesn't move
            agent.SetDestination(transform.position);

            if (!alreadyAttacked)
            {
                _attackAnimationStarter.StartAtack();
                // Attack code here
                Debug.Log("Alien attacks the player!");

                alreadyAttacked = true;
                Invoke(nameof(ResetAttack), timeBetweenAttacks);
            }
        }

        private void ResetAttack()
        {
            alreadyAttacked = false;
        }
    }
}
