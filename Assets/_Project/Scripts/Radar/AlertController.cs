using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace RtuItLab
{
    public class AlertController : MonoBehaviour
    {
        [SerializeField] private float initialDelay;

        [SerializeField] private List<Transform> monsters;

        [SerializeField] private float triggerDistance;

        private float _currentDelay;

        private AudioSource _audio; 
        void Start()
        {
            _currentDelay = initialDelay;
            _audio = GetComponent<AudioSource>();
        }

        void Update()
        {

            float multiplier = triggerDistance/getDistanceToNearestMonster();
            if(multiplier < 1f){
                multiplier = 1f;
            }
            _currentDelay -= Time.deltaTime * multiplier;
            if(_currentDelay <= 0f){
                _audio.Play();
                _currentDelay = initialDelay;
            }
        }

        private float getDistanceToNearestMonster(){
            float distance = float.PositiveInfinity;
            foreach (Transform monster in monsters){
                float distanceToCurrentMonster = (monster.position - transform.position).magnitude;
                if(distanceToCurrentMonster < distance){
                    distance = distanceToCurrentMonster;
                }
            }
            return distance;
        }
    }
}
