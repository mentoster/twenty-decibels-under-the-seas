using System.Collections;
using System.Collections.Generic;
using StarterAssets;
using UnityEngine;

namespace RtuItLab
{
    public class AtackAnimationStarter : MonoBehaviour
    {
        [SerializeField] private FirstPersonController firstPersonController;
        [SerializeField] private Collider playerCollider;

        [SerializeField] private Transform cameraRoot;
        [SerializeField] private float delay;

        [SerializeField] private Animator animator;

        [SerializeField] private Transform playerDeathPoint;

        private bool _atackStarted;

        [SerializeField] private float playerLerpSpeed;

        [SerializeField] private AudioSource killAudio;

        private void OnTriggerEnter(Collider other) {

            if (other == playerCollider){
                StartAtack();

            }
        }

        public void StartAtack()
        {
            firstPersonController.enabled = false;
            Invoke("StartAnimation", delay);
            _atackStarted = true;
            killAudio.Play();
        }

        private void StartAnimation(){
            animator.SetTrigger("atackStart");
        }

        private void Update() {
            if(_atackStarted){
                cameraRoot.transform.rotation = Quaternion.Lerp(cameraRoot.transform.rotation,
                    playerDeathPoint.transform.rotation, playerLerpSpeed * Time.deltaTime);
                cameraRoot.transform.position = Vector3.Lerp(cameraRoot.transform.position,
                    playerDeathPoint.transform.position, playerLerpSpeed * Time.deltaTime);
            }
        }
    }
}
