using System;
using UnityEditor;
using UnityEditor.Presets;
using UnityEngine;

public static class ApplyPresetTool
{
    [MenuItem("Assets/Apply presets", false, 1)]
    private static void ImportFolder()
    {
        foreach (var selectedGUID in Selection.assetGUIDs)
        {
            string path = AssetDatabase.GUIDToAssetPath(selectedGUID);
            if(AssetDatabase.IsValidFolder(path))
            {
                string[] guids = AssetDatabase.FindAssets(String.Empty, new[] { path });
                foreach (var guid in guids)
                {
                    ApplyPresetToAsset(AssetDatabase.GUIDToAssetPath(guid));
                }
            }
            else
            {
                ApplyPresetToAsset(path);
            }
        }
    }
    
    [MenuItem("Assets/Apply presets", true)]
    private static bool ValidateLogSelectedTransformName()
    {
        return Selection.assetGUIDs.Length > 0;
    }
    
    private static void ApplyPresetToAsset(string assetPath)
    {
        AssetImporter assetImporter = AssetImporter.GetAtPath(assetPath);
        
        Preset[] presets = Preset.GetDefaultPresetsForObject(assetImporter);
        int presetsLength = presets.Length;
        Preset lastAppliedAsset = null;
        
        for (int i = 0; i < presetsLength; i++)
        {
            Preset preset = presets[i];
            bool result = preset.ApplyTo(assetImporter);
            if (result)
            {
                lastAppliedAsset = preset;
            }
        }

        if (lastAppliedAsset != null)
        {
            AssetDatabase.ImportAsset(assetImporter.assetPath);
            Debug.Log($"Preset {lastAppliedAsset.name} applied to {assetImporter.assetPath}");
        }
    }
}